package com.deimer.villa.technicaltestvalid.di.components

import com.deimer.villa.technicaltestvalid.di.modules.RepositoryModule
import dagger.Component

@Component(modules = [RepositoryModule::class])
interface RepositoryComponent {

}