package com.deimer.villa.technicaltestvalid

import android.app.Application
import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.Locale

class TestApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        val locale = Locale("es","ES")
        Locale.setDefault(locale)
        setupRealmConfiguration()
    }

    companion object {
        private lateinit var instance: TestApplication

        fun getInstance(): TestApplication {
            return instance
        }

        fun getAppContext(): Context {
            return instance.applicationContext
        }
    }

    private fun setupRealmConfiguration() {
        val key = ByteArray(64)
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .name("TestApplication")
            .schemaVersion(0)
            .encryptionKey(key)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }
}