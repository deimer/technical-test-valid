package com.deimer.villa.technicaltestvalid.views.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.deimer.villa.technicaltestvalid.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
