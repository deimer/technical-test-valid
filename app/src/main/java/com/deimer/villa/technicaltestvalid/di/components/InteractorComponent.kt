package com.deimer.villa.technicaltestvalid.di.components

import com.deimer.villa.technicaltestvalid.di.modules.InteractorModule
import dagger.Component

@Component(modules = [InteractorModule::class])
interface InteractorComponent {

}