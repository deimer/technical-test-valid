package com.deimer.villa.technicaltestvalid.di.components

import com.deimer.villa.technicaltestvalid.di.modules.DataSourceModule
import dagger.Component

@Component(modules = [DataSourceModule::class])
interface DataSourceComponent {
}